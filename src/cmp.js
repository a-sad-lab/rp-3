import {fs, registerfn, unregisterfn} from './ft'

import * as sf from './ft'

function printfs() {
  console.log('fs', fs)
}

function addfn(fn, fnName) {
  registerfn(fn, fnName)
}

function minusfn(fnName) {
  unregisterfn(fnName)
  printfs()
}

export function Cmp() {
  return (
    <div>
      <h3>cmp</h3>
      <button onClick={printfs}>console.log</button>
      <button onClick={() => addfn(function added() {}, 'added')}>register fn</button>
      <button onClick={() => minusfn('a')}>unregister fn</button>
      <button onClick={() => console.log(fs['a'])}>show fs['a']</button>
      <hr />
      <button onClick={() => console.log(sf)}>show sf</button>
      <button onClick={() => sf['fs'] = 'sf'}>add to sf</button>
    </div>
  )
}
