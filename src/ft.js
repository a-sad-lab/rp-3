const fs = {
  a(state, payload) {
    return state + ' - a'
  },
  b(state, payload) {
    return state + ' - b'
  }
}

function registerfn(fn, fnDisplayName) {
  fs[fnDisplayName] = fn
}

function unregisterfn(fnName) {
  delete fs[fnName]
}

export {
  fs,
  registerfn,
  unregisterfn
}
